<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }
    public function auth(Request $request)
    {
        // dd($request);

        $namaDepan = strtoupper($request['first-name']);
        $namaBelakang = strtoupper($request['last-name']);


        return view('welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
