@extends('layouts.master')
@section('title')
    Halaman Edit
@endsection

@section('sub-title')
    Halaman Edit
@endsection

@push('styles')
    <link href="https://cdn.datatables.net/v/bs4/dt-1.13.4/datatables.min.css" rel="stylesheet" />
@endpush

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Edit Data</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="/cast/{{ $cast->id }}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" value="{{ $cast->nama }}" class="form-control">
                    <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Umur</label>
                        <input type="text" name="umur" value="{{ $cast->umur }}" class="form-control">
                </div>
                @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Bio</label>
                    <textarea type="text" name="bio" class="form-control">{{ $cast->bio }}</textarea>
                </div>
                @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <button type="submit" class="btn btn-primary">Submit</button> 
            </form>
            <a href="/cast/" class="btn btn-primary btn-md mt-2 mb-2">Kembali</a>
        </div>
        <!-- /.card-body -->
    @endsection
