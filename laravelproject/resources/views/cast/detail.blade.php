@extends('layouts.master')
@section('title')
    Detail
@endsection

@section('sub-title')
    Detail
@endsection

@push('styles')
    <link href="https://cdn.datatables.net/v/bs4/dt-1.13.4/datatables.min.css" rel="stylesheet" />
@endpush

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Detail</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <h1>{{ $cast->nama }}</h1>
            <h3>{{ $cast->umur }}</h3>
            <p>{{ $cast->bio }}</p>
            <a href="/cast/" class="btn btn-primary btn-md p-2 mb-2">Kembali</a>
        </div>
        <!-- /.card-body -->
    @endsection
