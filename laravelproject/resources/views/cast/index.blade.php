@extends('layouts.master')
@section('title')
    Cast
@endsection

@section('sub-title')
    Cast
@endsection

@push('styles')
    <link href="https://cdn.datatables.net/v/bs4/dt-1.13.4/datatables.min.css" rel="stylesheet" />
@endpush

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">DataTable with default features</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <a href="/cast/create" class="btn btn-primary btn-md p-2 mb-2">Tambah Data</a>
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Umur</th>
                        <th>Bio</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($cast as $key=> $cast)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $cast->nama }}</td>
                            <td>{{ $cast->umur }}</td>
                            <td>{{ $cast->bio }}</td>
                            <td>
                                <form action="/cast/{{ $cast->id }}" method="post" >
                                <a href="/cast/{{ $cast->id }}" class="btn btn-info btn-sm">Detail</a>
                                <a href="/cast/{{ $cast->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                               
                                    @csrf
                                    @method('delete')
                                    <input type="submit" value="delete" c class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @empty
                    @endforelse

                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
        </table>

        @push('scripts')
            <script src="{{ asset('/template/plugins/datatables/jquery.dataTables.js') }}"></script>
            <script src="{{ asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
            <script>
                $(function() {
                    $("#example1").DataTable();
                });
            </script>
        @endpush
    @endsection
