<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laravel</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sing Up</h2>
    <form action="/welcome" method="post">
        @csrf
        <label for="first-name">First name:</label>
        <input type="text" id="first-name" name="first-name"><br><br>
      
        <label for="last-name">Last name:</label>
        <input type="text" id="last-name" name="last-name"><br><br>
      
        <label>Gender:</label><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>
      
        <label for="nationality">Nationality:</label>
        <select id="nationality" name="nationality">
          <option value="indonesian">Indonesian</option>
          <option value="singapure">Singapure</option>
          <option value="malaysian">Malaysian</option>
        </select><br><br>
      
        <label>Languages spoken:</label><br>
        <input type="checkbox" id="bahasa-indonesia" name="languages-spoken" value="bahasa-indonesia">
        <label for="bahasa-indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="languages-spoken" value="english">
        <label for="english">English</label><br>
        <input type="checkbox" id="other-languages" name="languages-spoken" value="other-languages">
        <label for="other-languages">Other</label><br><br>
      
        <label for="bio">Bio:</label><br>
        <textarea id="bio" name="bio"></textarea><br><br>
      
        <label for="caption">Enter a caption for this image:</label><br>
        <input type="text" id="caption" name="caption"><br><br>
      
        <input type="submit" value="Sign Up">
      </form>
      
</body>
</html>